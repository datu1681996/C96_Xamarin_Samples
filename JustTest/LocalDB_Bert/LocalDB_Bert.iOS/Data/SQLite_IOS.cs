﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Foundation;
using LocalDB_Bert.Data;
using LocalDB_Bert.iOS.Data;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLite_IOS))]

namespace LocalDB_Bert.iOS.Data
{
    public class SQLite_IOS: ISQLite
    {
        public SQLite_IOS(){ }
        public SQLite.SQLiteConnection GetConnection()
        {
            var sqliteFileName = "Testdb.db3";
            string documentsPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var libraryPath = Path.Combine(documentsPath, "..", "Library");
            var path = Path.Combine(libraryPath, sqliteFileName);
            var connection = new SQLite.SQLiteConnection(path);

            return connection;
        }
    }
}