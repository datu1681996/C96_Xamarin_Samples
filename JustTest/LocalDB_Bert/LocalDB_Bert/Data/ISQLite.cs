﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace LocalDB_Bert.Data
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}
