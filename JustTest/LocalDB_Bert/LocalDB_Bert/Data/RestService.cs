﻿using LocalDB_Bert.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LocalDB_Bert.Data
{
    //Tutorial part 5: restAPI connection
    public class RestService
    {
        HttpClient client;
        string grant_type = "password";

        public RestService()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            client.DefaultRequestHeaders
                .Accept
                .Add(new System.Net.Http.Headers
                                       .MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
        }

        public async Task<Token> Login(User user)
        {
            var postData = new List<KeyValuePair<string, string>>();
            //for UI
            postData.Add(new KeyValuePair<string, string>("grant_type", grant_type));
            //for logic part
            postData.Add(new KeyValuePair<string, string>("username", user.Username));
            postData.Add(new KeyValuePair<string, string>("password", user.Password));

            var weburl = "www.test.com";
            var content = new FormUrlEncodedContent(postData);
            var response = await PostResponseLogin<Token>(weburl, content);
            DateTime dt = new DateTime();
            dt = DateTime.Today;
            response.expire_date = dt.AddSeconds(response.expire_in); //if date expire 
                                                                      //-> then re-login & get new token

            return response;
        }

        public async Task<T> PostResponseLogin<T>(string weburl, FormUrlEncodedContent content) where T : class
        {
            //this func to loggin to server -> get token 

            var response = await client.PostAsync(weburl, content);
            var jsonResult = response.Content.ReadAsStringAsync().Result;
            //var token = JsonConvert.DeserializeObject<Token>(jsonResult); --> initial code
            var responseObject = JsonConvert.DeserializeObject<T>(jsonResult);

            return responseObject;
        }

        //use the token for authorization of API call
        public async Task<T> PostResponse<T>(string weburl, string jsonstring) where T : class// para used for input instead of encoded content
        {
            var Token = App.TokenDatabase.GetToken();
            string ContentType = "application/json";
            client.DefaultRequestHeaders
                  .Authorization = new System.Net
                                            .Http
                                            .Headers
                                            .AuthenticationHeaderValue("Bearer", Token.access_Token);
            try
            {
                var Result = await client.PostAsync(weburl, new StringContent(jsonstring, Encoding.UTF8, ContentType));
                if (Result.StatusCode == System.Net.HttpStatusCode.OK) // improved part of code
                {
                    var jsonResult = Result.Content.ReadAsStringAsync().Result;
                    try
                    {
                        var ContentResp = JsonConvert.DeserializeObject<T>(jsonResult);
                        return ContentResp;
                    }
                    catch{ return null; }
                }
            }
            catch{ return null; }

            return null;
        }

        public async Task<T> GetResponse<T>(string weburl) where T : class
        {
            var Token = App.TokenDatabase.GetToken();
            client.DefaultRequestHeaders
                  .Authorization = new System.Net
                                            .Http
                                            .Headers
                                            .AuthenticationHeaderValue("Bearer", Token.access_Token);
            try
            {
                var response = await client.GetAsync(weburl);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var JsonResult = response.Content.ReadAsStringAsync().Result;
                    try
                    {
                        var ContentResp = JsonConvert.DeserializeObject<T>(JsonResult);
                        return ContentResp;
                    }
                    catch { return null;}

                }

            }
            catch { return null;}

            return null;
        }

    }

}
