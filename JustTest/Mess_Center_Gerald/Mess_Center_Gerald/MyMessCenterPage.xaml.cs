﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mess_Center_Gerald
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MyMessCenterPage : ContentPage
	{
        private string _tickContract = "tick";
        //refactor
        //public ObservableCollection<string> Events { get; set; } = new ObservableCollection<string>();
        MyMessCenterViewModel messVM = new MyMessCenterViewModel();
        public MyMessCenterPage ()
		{
			InitializeComponent ();

            BindingContext = messVM;
        }

        void HandleFire_Clicked(Object sender, System.EventArgs e)
        {
            //send message
            MessagingCenter.Send(this, _tickContract, DateTime.Now);
        }

        void HandleSubcribe_Clicked(Object sender, System.EventArgs e)
        {
            //Subscribe message
            MessagingCenter.Subscribe<MyMessCenterPage, DateTime>(this, _tickContract, (s, a) =>
            {
                messVM.Events.Add($"Receive message at{a.ToString()}");
            });
        }

        void HandleClear_Clicked(Object sender, System.EventArgs e)
        {
            //clear message
            messVM.Events.Clear();
        }

    }
}