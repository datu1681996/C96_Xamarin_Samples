﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Mess_Center_Gerald
{
    public class MyMessCenterViewModel
    {
        public ObservableCollection<string> Events { get; set; } = new ObservableCollection<string>();
    }
}
