﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mvvm_Houssem.Models
{
    public class TaskModel
    {
        public string Title { get; set; }
        public int Duration { get; set; }
    }
}
