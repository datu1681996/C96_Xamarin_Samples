﻿using Mvvm_Houssem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Mvvm_Houssem.Annotations;
using Xamarin.Forms;

namespace Mvvm_Houssem.ViewModels
{
    public class HomeViewModel : INotifyPropertyChanged
    {
        private TaskModel _taskModel;
        private string _message;  //Part 13 [MVVM Command]

        public TaskModel TaskModel
        {
            get => _taskModel;
            set => _taskModel = value;
        }

        #region Part 13 [MVVM Command]

        public string Message
        {
            get => _message;
            set
            {
                _message = value;
                OnPropertyChanged();
            } 
        }

        public Command SaveCommand
        {
            get
            {
                return  new Command(() =>
                {
                    Message = "Your task: " + TaskModel.Title + " ," 
                              + TaskModel.Duration + " was successfully save!";
                });
            }
        }

        #endregion

        public HomeViewModel()
        {
            TaskModel = new TaskModel
            {
                Title = "create UI",
                Duration = 2,
            };
        }


        #region Part 13 [MVVM Command]

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
